package strategies
{
	import com.greensock.TweenMax;
	import strategies.IMoveVehicleBehavior;
	import vehicles.Vehicle;

	public class Teleport implements IMoveVehicleBehavior 
	{		
		public function move(vehicle:Vehicle, tweenVars:Object):void 
		{
			TweenMax.to(vehicle, 0, tweenVars);
		}		
	}
}