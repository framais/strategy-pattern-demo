package strategies
{
	import strategies.IMoveVehicleBehavior;
	import com.greensock.easing.Quad;
	import com.greensock.TweenMax;
	import vehicles.Vehicle;

	public class Fly implements IMoveVehicleBehavior
	{		
		public function move(vehicle:Vehicle, tweenVars:Object):void 
		{
			tweenVars.ease = Quad.easeInOut;
			TweenMax.to(vehicle, .5, tweenVars);			
		}		
	}
}