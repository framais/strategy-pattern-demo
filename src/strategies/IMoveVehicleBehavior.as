package strategies
{
	import vehicles.Vehicle;
	
	public interface IMoveVehicleBehavior 
	{
		function move(vehicle:Vehicle, tweenVars:Object):void;
	}
}