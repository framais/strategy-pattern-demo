package 
{
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import strategies.Fly;
	import strategies.Swim;
	import strategies.Teleport;
	import strategies.Walk;
	import vehicles.Raven;
	
	public class Main extends Sprite 
	{
		private var raven:Raven;
		
		public function Main():void 
		{
			raven = new Raven();
			
			raven.moveBehavior = new Fly();
			
			raven.x = 100;
			raven.y = 100;
			
			addChild(raven);
			
			stage.addEventListener(MouseEvent.CLICK, onMouseClick);
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
		}
		
		private function onMouseClick(event:MouseEvent):void 
		{
			var tweenVars:Object = { x:event.stageX, y:event.stageY, scaleX:1, scaleY:1};
			raven.move(tweenVars);
		}
		
		private function onKeyUp(event:KeyboardEvent):void
		{
			switch (event.keyCode)
			{
				case 49: // keyboard 1
					trace("Fly!");
					raven.moveBehavior = new Fly();
					break;
					
				case 50: // keyboard 2
					trace("Walk!");
					raven.moveBehavior = new Walk();
					break;
					
				case 51: // keyboard 3
					trace("Swim!");
					raven.moveBehavior = new Swim();
					break;
					
				case 52: // keyboard 4
					trace("Teleport!");
					raven.moveBehavior = new Teleport();
					break;					
			}
		}		
	}
}