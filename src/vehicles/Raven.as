package vehicles
{
	import vehicles.Vehicle;
	
	public class Raven extends Vehicle 
	{
		public function Raven() 
		{
			graphics.beginFill(0x00cc00);
			graphics.drawRect(0, 0, 20, 20);
			graphics.endFill();				
		}		
	}
}