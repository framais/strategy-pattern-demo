package vehicles
{
	import strategies.IMoveVehicleBehavior;
	import flash.display.Sprite;
	
	public class Vehicle extends Sprite 
	{
		public var moveBehavior:IMoveVehicleBehavior;
		
		public function move(tweenVars:Object):void
		{
			moveBehavior.move(this, tweenVars);
		}
	}
}